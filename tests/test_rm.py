# -*- coding: utf-8 -*-

import os
import os.path
import unittest
import shutil
import tempfile

import smartrm.config as cf
import smartrm.rm as rm
from smartrm.trash import Trash
from smartrm.rmerror import RMError


class TestRm(unittest.TestCase):
	def setUp(self):
		config = cf.get_default()

		trash_path = tempfile.mkdtemp()

		config.trash_path = trash_path

		self.trash = Trash(config)


	def tearDown(self):
		shutil.rmtree(self.trash.location)


	def test_rmfile(self):
		file_1 = tempfile.NamedTemporaryFile()
		file_2 = tempfile.NamedTemporaryFile()

		print(file_1.name)
		print(file_2.name)

		rm.remove(file_1.name)

		rm.remove(file_2.name, trash=self.trash)

		self.assertFalse(os.path.exists(file_1.name), 
			             'Не работает удаление файла')

		self.assertFalse(os.path.exists(file_2.name), 
			             'Не работает удаление файла в корзину')


	def test_rmdir(self):
		dir_path_1 = tempfile.mkdtemp()
		dir_path_2 = tempfile.mkdtemp()

		rm.remove_directory(dir_path_1)
		rm.remove_directory(dir_path_2, self.trash)

		self.assertFalse(os.path.exists(dir_path_1),
			             'Не работает удаление директории')
		self.assertFalse(os.path.exists(dir_path_2),
			             'Не работает удаление директории в корзину')

		dir_path = tempfile.mkdtemp()
		file = tempfile.NamedTemporaryFile(dir=dir_path)

		with self.assertRaises(RMError):
			rm.remove_directory(dir_path)

		with self.assertRaises(RMError):
			rm.remove_directory(dir_path, self.trash)

		shutil.rmtree(dir_path)


	def test_remove_tree(self):
		dir_path_1 = tempfile.mkdtemp()
		dir_path_2 = tempfile.mkdtemp()
		file_1 = tempfile.NamedTemporaryFile(dir=dir_path_1)
		file_2 = tempfile.NamedTemporaryFile(dir=dir_path_2)

		rm.remove(dir_path_1)
		rm.remove(dir_path_2, self.trash)

		self.assertFalse(os.path.exists(dir_path_1), 'Не удаляет дерево')
		self.assertFalse(os.path.exists(dir_path_2), \
			             'Не удаляет дерево в корзину')


	def test_remove_from_regex(self):
		dir_path = tempfile.mkdtemp()
		sub_dir_path = tempfile.mkdtemp(dir=dir_path)

		file_1 = tempfile.NamedTemporaryFile(prefix='Foo', dir=dir_path)
		file_2 = tempfile.NamedTemporaryFile(prefix='Bar', dir=dir_path)
		file_3 = tempfile.NamedTemporaryFile(prefix='Foo', dir=sub_dir_path)

		rm.remove_from_regex(dir_path, 'Foo.*')

		self.assertFalse(os.path.exists(file_1.name),
			             'Не удалило нужный файл')
		self.assertFalse(os.path.exists(file_3.name),
			             'Не удалило нужный файл')
		self.assertTrue(os.path.exists(file_2.name),
			            'Удалило нужный файл')

		file_1 = tempfile.NamedTemporaryFile(prefix='Foo', dir=dir_path)
		file_3 = tempfile.NamedTemporaryFile(prefix='Foo', dir=sub_dir_path)

		rm.remove_from_regex(dir_path, 'Foo.*', self.trash)

		self.assertFalse(os.path.exists(file_1.name),
			             'Не удалило нужный файл')
		self.assertFalse(os.path.exists(file_3.name),
			             'Не удалило нужный файл')
		self.assertTrue(os.path.exists(file_2.name),
			            'Удалило нужный файл')

		shutil.rmtree(dir_path)


if __name__ == '__main__':
	unittest.main()