# -*- coding: utf-8 -*-

import os
import os.path
import unittest
import time
import shutil
import tempfile

import smartrm.config as cf
from smartrm.trash import Trash
from smartrm.rmerror import RMError


class TestTrash(unittest.TestCase):
	def setUp(self):
		config = cf.get_default()

		trash_path = tempfile.mkdtemp()

		config.trash_path = trash_path

		self.trash = Trash(config)


	def tearDown(self):
		shutil.rmtree(self.trash.location)


	def test_add_one(self):
		file = tempfile.NamedTemporaryFile()

		index = self.trash.add(file.name)

		self.assertFalse(os.path.exists(file.name), 
			             'Файл не удалён из начальной директории')

		trash_path = os.path.join(self.trash._files_location, \
			                      os.path.basename(file.name) + \
			                      '[{0}]'.format(index))

		self.assertTrue(os.path.exists(trash_path), \
			            'Файл не перемещён в корзину')


	def test_restore_file(self):
		file = tempfile.NamedTemporaryFile()

		index = self.trash.add(file.name)

		self.trash.pop(os.path.basename(file.name), index)

		self.assertTrue(os.path.exists(file.name), 'Файл не восстановлен')

		if os.path.exists(self.trash._files_location):
			self.assertEqual(os.listdir(self.trash._files_location), [],
				             'Файл не удалён из корзины')

		if os.path.exists(self.trash._info_location):
			self.assertEqual(os.listdir(self.trash._info_location), [],
				             '.trashinfo не удалён из корзины')


	def test_restore_nonexists_file(self):
		file = tempfile.NamedTemporaryFile()

		index = self.trash.add(file.name)
		
		with self.assertRaises(RMError):
			self.trash.pop(os.path.basename(file.name) + '[0]', 0)


	def test_autoclear(self):
		files = [tempfile.NamedTemporaryFile(bufsize=0) for it in range(4)]

		self.trash.config.trash_politics['maxsize'] = 100
		self.trash.config.trash_politics['maxtime'] = 2

		self.trash._setup_clear_politics()

		files[0].write('a' * 1000)
		files[2].write('a' * 1000)

		indexs = list()

		indexs.append(self.trash.add(files[0].name))
		indexs.append(self.trash.add(files[1].name))

		time.sleep(2)

		indexs.append(self.trash.add(files[2].name))
		indexs.append(self.trash.add(files[3].name))

		self.trash._autoclear()

		self.assertFalse(
			os.path.exists(
				os.path.join(
					self.trash._files_location,
					os.path.basename(files[1].name) + '[{0}]'.format(indexs[1])
				)
			),
			'Не работает удаление по maxtime'
		)

		self.assertFalse(
			os.path.exists(
				os.path.join(
					self.trash._files_location,
					os.path.basename(files[2].name) + '[{0}]'.format(indexs[2])
				)
			),
			'Не работает удаление по maxsize'
		)

		self.assertFalse(
			os.path.exists(
				os.path.join(
					self.trash._files_location, 
					os.path.basename(files[0].name) + '[{0}]'.format(indexs[0])
			    )
			),
			'Не работает удаление по обеим политикам'
		)

		self.assertTrue(
			os.path.exists(
				os.path.join(
					self.trash._files_location,
					os.path.basename(files[3].name) + '[{0}]'.format(indexs[3])
				)
			),
			'Удаляет хороший файл'
		)


if __name__ == "__main__":
	unittest.main()
