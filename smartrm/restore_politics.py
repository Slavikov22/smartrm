# -*- coding: utf-8 -*-

import os.path

from smartrm.rmerror import RMError

def politic(name):
	def selective(operation):
		collisions = []
		for file in operation.files:
			if os.path.exists(file.path):
				collisions.append(file)

		if collisions:
			print 'Detected {0} collisions : '.format(len(collisions))
			for file in collisions:
				print ' '*4 + file.path 


			print 'Do you want to restore files?'
			if raw_input() != 'y':
				raise RMError(str(operation.id) + ' : Find collisions')


	def passive(operation):
		for file in operation.files:
			if os.path.exists(file.path):
				raise RMError(str(operation.id) + ' : Find collisions')


	def agressive(operation):
		pass


	if name == 'selective':
		return selective
	elif name == 'passive':
		return passive
	elif name == 'agressive':
		return agressive
