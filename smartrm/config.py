# -*- coding: utf-8 -*-

import os
import os.path

import smartrm.helpers.json_helper as json_helper
import smartrm.helpers.config_helper as config_helper
from smartrm.rmerror import RMError


_possible_choise_politics = ('passive', 'agressive', 'selective')


def get_default():
    class Config(object):
        def __init__(self):
            self.trash_path = os.path.expanduser('~/.trash')
            self.choise_politic = 'selective'
            self.trash_politics = {
                'maxsize' : 0,
                'maxtime' : 0
            }

    return Config()


def get_by_user():
    path = os.path.expanduser('~/.trashconfig')

    if os.path.exists(path):
        config = get_by_file(path)      
    else:
        config = get_default()
        save(config, path)

    return config


def get_by_file(path, form='config'):
    if form == 'json': 
        data = json_helper.load(path)
    elif form == 'config':
        data = config_helper.load(path)

    config = get_default()
    for key, value in data.items():
        setattr(config, key, value)

    if not config.choise_politic in _possible_choise_politics:
        raise RMError('Incorrect choise politic')

    return config


def save(config, path, form='config'):
    data = {}

    for field in dir(config):
        if not field.startswith('__'):
            data[field] = getattr(config, field)

    if form == 'json':
        json_helper.save(data, path)
    elif form == 'config':
        config_helper.save(data, path)
    