# -*- coding: utf-8 -*-

import argparse
import os
import os.path
import logging
import sys
import pickle

import smartrm.logger
import smartrm.rm as rm
import smartrm.config as config
from smartrm.trash import Trash
from smartrm.rmerror import RMError
from smartrm.operation import Operation


class Parser(object):

    def __init__(self):
        self.init_operations()

        self.parser = argparse.ArgumentParser()

        self.parser.add_argument('files', nargs='*')

        self.parser.add_argument('-t', '--trash', action='store_true')
        self.parser.add_argument('-d', '--dir', action='store_true')
        self.parser.add_argument('-r', '--recursive', action='store_true')
        self.parser.add_argument('--regex', default='')

        self.parser.add_argument('-R', '--restore', default='')

        self.parser.add_argument('-o', '--operations', action='store_true')
        self.parser.add_argument('--del-op', dest='del_op', default='')

        self.politic_group = self.parser.add_mutually_exclusive_group()

        self.politic_group.add_argument('-i', '--interactive', 
                                        action='store_true')
        self.politic_group.add_argument('-f', '--force', 
                                        action='store_true')
        self.politic_group.add_argument('--check-system', dest='check_system',
                                        action='store_true')
        self.politic_group.add_argument('-s', '--silent', action='store_true')

        self.parser.add_argument('-l', '--log', action='count')
        self.parser.add_argument('--log-file', dest='log_file', default='')

        self.parser.add_argument('--trash-show', dest='trash_show', 
                                 action='store_true')
        self.parser.add_argument('--trash-clear', dest='trash_clear',
                                 action='store_true')

        self.parser.add_argument('--path-config', dest='path_config', 
                                 default='')
        self.parser.add_argument('--local', nargs='*', default=[])
        self.parser.add_argument('--global', dest='glob', nargs='*',
                                 default=[])
        self.parser.add_argument('--format', dest='form', default='config')

        self.parser.add_argument('--dry-run', dest='dry_run', 
                                 action='store_true')


    def init_operations(self):
        self.path_operations = os.path.join(os.path.dirname(__file__),
                               '../data/operations')

        try:
            with open(self.path_operations, 'r') as file:
                self.operations = pickle.load(file)
        except EOFError:
            self.operations = list()


    def parse(self):
        arg = self.parser.parse_args()

        try:
            logger = self.get_logger(arg)
        except RMError as exc:
            arg.log_file = None
            logger = self.get_logger(arg)
            logger.error(exc.message)
        except Exception:
            sys.exit(1)

        try:
            config = self.get_config(arg)
                
            trash = self.get_trash(arg, config)

            if arg.trash_show:
                trash.show()
            elif arg.trash_clear:
                trash.clear()
            elif arg.del_op or arg.restore:
                if arg.restore:
                    operation = self.get_operation(arg.restore)
                else:
                    operation = self.get_operation(arg.del_op)

                index = 0
                for it in range(len(self.operations)):
                    if self.operations[it].id == operation.id:
                        index = it 

                if not trash:
                    trash = Trash(config)

                if arg.restore:
                    try:
                        rm.restore(operation, trash, arg.dry_run)
                    except RMError as exc:
                        logger.error(exc.message)
                    else:
                        del self.operations[index]
                else:
                    try:
                        rm.delete_operation(operation, trash, arg.dry_run)
                    except RMError as exc:
                        logger.error(exc.message)

                        if not silent:
                            print exc.message
                            print 'Continue?'
                            if input() == 'y':
                                del self.operations[index]
                        del self.operations[index]

                if not arg.dry_run:
                    with open(self.path_operations, 'w') as file:
                        pickle.dump(self.operations, file)
            elif arg.operations:
                if trash:
                    operations = [op for op in self.operations \
                                  if op.trash_path == trash.location]
                else:
                    operations = self.operations

                print '_'*26
                for operation in operations: 
                    operation.show()
            else:
                self.setup_politic(arg)
                self.process_files(arg, trash, logger, arg.silent)

            sys.exit(0)
        except RMError as exc:
            if not arg.silent: 
                print exc.message
            sys.exit(1)
        except Exception:
            if not arg.silent:
                print 'System error'
            sys.exit(1)


    def get_logger(self, arg):
        if arg.silent:
            level = logging.CRITICAL
        elif not arg.log:
            level = logging.CRITICAL
        elif arg.log == 1:
            level = logging.INFO
        else:
            level = logging.DEBUG

        if arg.log_file:
            smartrm.logger.init(level=level, filename=arg.log_file)
        else:
            smartrm.logger.init(level=level, filename=arg.log_file)

        return logging.getLogger('SmartRM')


    def get_config(self, arg):
        if arg.path_config:
            conf = config.get_by_file(arg.path_config, form=arg.form)
        else:
            conf = config.get_by_user()

        for line in arg.glob:
            option, value = line.split('=')
            if '.' in option:
                option, suboption = option.split('.')
                if value.isdigit():
                    value = int(value)

                if option not in dir(conf):
                    setattr(conf, option, dict())

                conf.__dict__[option][suboption] = value
            else:
                setattr(conf, option, value)
                conf.option = value

        conf.trash_path = os.path.abspath(conf.trash_path)

        if not arg.path_config:
            config.save(conf, os.path.expanduser('~/.trashconfig'))
        else:
            config.save(conf, arg.path_config, form=arg.form)

        for line in arg.local:
            option, value = line.split('=')
            if '.' in option:
                option, suboption = option.split('.')
                if value.isdigit():
                    value = int(value)

                if option not in dir(conf):
                    setattr(conf, option, dict())

                conf.__dict__[option][suboption] = value
            else:
                setattr(conf, option, value)

        if arg.silent:
            conf.trash_politic = 'passive'

        return conf


    def get_trash(self, arg, config):
        if arg.trash or arg.restore or arg.trash_show or arg.trash_clear:
            return Trash(config)
        else:
            return None


    def get_operation(self, ident):
        operations = [op for op in self.operations \
                      if str(op.id).startswith(ident)]

        if len(operations) > 1:
            raise RMError(ident + ' : Need more information')
        elif len(operations) == 0:
            raise RMError(ident + ' : Operation not found')
        else:
            return operations[0]


    def setup_politic(self, arg):
        if arg.silent or arg.force:
            rm.setup_politic('force')
        elif arg.interactive:
            rm.setup_politic('interactive')
        else:
            rm.setup_politic('check_system')


    def process_files(self, arg, trash, logger, silent):
        for file in arg.files:
            try:
                if os.path.isfile(file) or os.path.islink(file):
                    rm.remove_file(file, trash, arg.dry_run)
                elif os.path.isdir(file):
                    if arg.regex:
                        rm.remove_from_regex(file, arg.regex, trash, 
                                             arg.dry_run, silent)
                    elif arg.recursive:
                        rm.remove_tree(file, trash, arg.dry_run,)
                    elif arg.dir:
                        rm.remove_directory(file, trash, arg.dry_run)
                    else:
                        raise RMError(file + \
                              ' : Impossible to delete directory')
                else:
                    raise RMError(file + ' : Not a file or directory')
            except RMError as exc:
                if not silent:
                    if logger:
                        logger.error(exc.message)
                    print exc.message

        if not arg.dry_run:
            if arg.trash and rm.operation.files:
                operation = rm.operation

                for file in operation.files:
                    file.path

                operation.trash_path = trash.location
                self.operations.append(operation)

                logger.info(str(operation.id) + ' : Operation append')

            with open(self.path_operations, 'w') as file:
                pickle.dump(self.operations, file)


def run():
    parser = Parser()
    parser.parse()


if __name__ == '__main__':
    run()
