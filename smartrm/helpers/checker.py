# -*- coding: utf-8 -*-

import os
import os.path

from smartrm.rmerror import RMError

def check_file(path):
    if not os.path.exists(path):
        raise RMError(path + ' : File not found')

    if not os.access(path, os.R_OK):
        raise RMError(path + ' : Permission denied')

    if not os.path.isfile(path):
        raise RMError(path + ' : Is not a file')


def check_dir(path):
    if not os.path.exists(path):
        raise RMError(path + ' : Directory not found')

    if not os.access(path, os.R_OK):
        raise RMError(path + ' : Permission denied')

    if not os.path.isdir(path):
        raise RMError(path + ' : Is not a directory')
