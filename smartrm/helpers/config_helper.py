# -*- coding: utf-8 -*-

import ConfigParser
import os
import os.path

from smartrm.rmerror import RMError

def load(path):
    if not os.path.exists(path):
        raise RMError(path + ' : File not found')

    if not os.path.isfile(path):
        raise RMError(path + ' : Is not file')

    if not os.access(path, os.R_OK):
        raise RMError(path + ' : Permission denied')

    data = {}

    parser = ConfigParser.ConfigParser()
    parser.read(path)

    for section in parser.sections():
        if section == 'config':
            for option in parser.options(section):
                data[option] = parser.get(section, option)

                if data[option].isdigit():
                    data[option] = int(data[option])
        else:
            data[section] = {}
            for option in parser.options(section):  
                data[section][option] = parser.get(section, option)

                if data[section][option].isdigit():
                    data[section][option] = int(data[section][option])

    return data


def save(data, path):
    if os.path.exists(path):
        if not os.path.isfile(path):
            raise RMError(path + ' : Is not file')

        if not os.access(path, os.W_OK):
            raise RMError(path + ' : Permission denied')

    parser = ConfigParser.ConfigParser()
    parser.add_section('config')

    for key in data.keys():
        if isinstance(data[key], dict):
            parser.add_section(key)
            for subkey in data[key].keys():
                parser.set(key, subkey, data[key][subkey])
        else:
            parser.set('config', key, data[key])

    with open(path, 'w') as file:
        parser.write(file)
