# -*- coding: utf-8 -*-

import json
import os
import os.path

from smartrm.rmerror import RMError


def load(path):
    if not os.path.exists(path):
        raise RMError(path + ' : File not found')

    if not os.path.isfile(path):
        raise RMError(path + ' : Is not file')

    if not os.access(path, os.R_OK):
        raise RMError(path + ' : Permission denied')

    decoder = json.JSONDecoder()

    with open(path, 'r') as file:
       data = decoder.decode(file.read())

    return data


def save(data, path):
    if os.path.exists(path):
        if not os.path.isfile(path):
            raise RMError(path + ' : Is not file')

        if not os.access(path, os.W_OK):
            raise RMError(path + ' : Permission denied')

    encoder = json.JSONEncoder(indent=4)
    data = encoder.encode(data)

    with open(path, 'w') as file:
        file.write(data)
        