# -*- coding: utf-8 -*-

import os
import os.path
import time
import ConfigParser
import shutil
import logging

import smartrm.trash_politics as politics
import smartrm.config as cf
from smartrm.file import File
from smartrm.rmerror import RMError


class Trash(object):
    def __init__(self, config=None):
        self.logger = logging.getLogger("SmartRM")

        self.logger.debug('Initialization trash ...')

        self.config = config or cf.get_by_user()

        self.location = self.config.trash_path
        self._files_location = os.path.join(self.location, 'files/')
        self._info_location = os.path.join(self.location, 'info/')

        self._init_dir(self.location)
        self._init_dir(self._files_location)
        self._init_dir(self._info_location)

        self._init_files()
        self._init_info()

        self._setup_clear_politics()
        self._autoclear()

        self.logger.debug('Initialization trash successful')


    def add(self, path, files_queue=None):
        self.logger.info(path + ' : Try to move to trash ...')

        if path.startswith(self.location) or self.location.startswith(path):
            raise RMError('Try to delete trash files')

        if os.path.isdir(path):
            if not os.access(path, os.R_OK):
                raise RMError(path + ' : Permission denied')
            
            if os.listdir(path):
                raise RMError(path + ' : Dir is not empty')

        file = File().get_by_file(path)

        if files_queue:
            self._files = files_queue.get()
            index = self._add_to_files(file)
            files_queue.put(self._files)
        else:
            index = self._add_to_files(file)

        info_parser = file.get_info_parser()

        with open(self._info_location + file.name + \
                  '[{0}]'.format(index) + ".trashinfo", "w") as info_file:
            info_parser.write(info_file)

        os.rename(path, self._files_location + file.name + 
                  '[{0}]'.format(index))

        self.logger.info(path + ' : Successfully move to trash')

        return index


    def pop(self, filename, index):
        self.logger.info(filename + ' : Try to restore...')

        file_path = self._files_location + filename + \
                    '[{0}]'.format(index)
        info_path = self._info_location + filename + \
                    '[{0}]'.format(index) + '.trashinfo'

        try:
            file = self._files[filename][index]
            path = file.path
        except Exception:
            raise RMError(filename + '[{0}]'.format(index) + \
                          ' : File not found in trash')

        if os.path.isdir(path) and not os.access(path, os.R_OK):
            raise RMError(path + ' : Permission denied')

        if not(os.path.isdir(path) and os.path.isdir(file_path)):
            self._replace(file_path, path)
        else:
            self._remove(file_path)

        self._remove(info_path)

        self._files[filename][index] = None

        self.logger.info(path + ' : Restoring successful')

        return 0


    def delete(self, filename, index):
        self.logger.info(filename + ' : Try to delete from trash...')

        if not filename in self._files.keys() or \
           len(self._files[filename]) <= index:
            raise RMError(filename + '[{0}]'.format(index) + ' : Not found')

        file_path = self._files_location + filename + \
                    '[{0}]'.format(index)
        info_path = self._info_location + filename + \
                    '[{0}]'.format(index) + '.trashinfo'

        self._remove(file_path)
        self._remove(info_path)

        self._files[filename][index] = None

        self.logger.info(filename + ' : Deleted successful')
        

    def clear(self):
        self.logger.info('Clearing trash...')

        for filename in self._files.keys():
            for index in range(len(self._files[filename])):
                file = self._files[filename][index]
                if file:
                    self.delete(file.name, index)

        self.logger.info('Trash is clear')


    def show(self):
        for filename in self._files.keys():
            for index in range(len(self._files[filename])):
                file = self._files[filename][index]
                if file:
                    print 'name :', file.name
                    print 'index :', index
                    print 'path :', file.path
                    print 'size :', file.size
                    print 'time :', int(time.time() - file.deletion_time)
                    print ''


    def _add_to_files(self, file, index=None):
        filename = file.name

        if filename not in self._files.keys():
            self._files[filename] = list()

        if index == None:
            if None in self._files[filename]:
                index = self._files[filename].index(None)
            else:
                index = len(self._files[filename])
                self._files[filename].append(None)
        else:
            while index >= len(self._files[filename]):
                self._files[filename].append(None)

        self._files[filename][index] = file

        return index


    def _autoclear(self):
        self.logger.debug('Autoclear is starting...')

        for filename in self._files.keys():
            for index in range(len(self._files[filename])):
                file = self._files[filename][index]
                if file:
                    for checker in self._checkers:
                        if checker(file):
                            self.delete(filename, index)
                            break

        self.logger.debug('Autoclear finished')


    def _init_dir(self, path):
        self.logger.debug(path + ' : Initialization directory...')

        if os.path.exists(path):
            if not os.path.isdir(path):
                raise RMError(path + ': Is not a directory')

            if not os.access(path, os.R_OK):
                raise RMError(path + ': Permission denied')
        else:
            os.mkdir(path)

        self.logger.debug(path + ' : Initialization directory successful')


    def _init_files(self):
        self.logger.debug('Initialization trash files...')

        self._files = {}

        info_parser = ConfigParser.RawConfigParser()

        for filename in os.listdir(self._files_location):
            try:
                pos = filename.rfind('[')
                index = int(filename[pos+1:-1])
                filename = filename[:pos]
            except Exception:
                self._remove(self._files_location + filename)
            else:
                file_path = self._files_location + filename + \
                            '[{0}]'.format(index)
                info_path = self._info_location + filename + \
                            '[{0}]'.format(index) + '.trashinfo'

                if os.path.exists(info_path):
                    try:
                        info_parser.read(info_path)
                    except Exception:
                        self._remove(file_path)

                    file = File().get_by_info(info_path)
                    self._add_to_files(file, index)
                else:
                    self._remove(file_path)

        self.logger.debug('Initialization trash files successful')


    def _init_info(self):
        self.logger.debug('Initialization info files...')

        for infoname in os.listdir(self._info_location):
            if not os.path.exists(self._files_location + \
                                  infoname.rstrip('.trashinfo')):
                self._remove(self._info_location + infoname)

        self.logger.debug('Initialization info files successful')


    def _replace(self, path, fpath):
        if os.path.exists(fpath):
            self._remove(fpath)
            
        os.renames(path, fpath)

        return 0


    def _remove(self, path):
        if os.path.islink(path):
            try:
                os.remove(path)
            except OSError:
                raise RMError(path + ' : Permission denied')
        elif os.path.isfile(path):
            if not os.access(path, os.R_OK):
                raise RMError(path + ' : Permission denied')
            os.remove(path)
        elif os.path.isdir(path):
            if not os.access(path, os.R_OK):
                raise RMError(path + ' : Permission denied')
            shutil.rmtree(path)

        return 0


    def _setup_clear_politics(self):
        self._checkers = list()

        maxsize = self.config.trash_politics['maxsize']
        maxtime = self.config.trash_politics['maxtime']

        if maxsize:
            self._checkers.append(politics.clear_checker('maxsize', maxsize))

            self.logger.debug('Clear politic maxsize is setup with ' + \
                              str(maxsize))

        if maxtime:
            self._checkers.append(politics.clear_checker('maxtime', maxtime))

            self.logger.debug('Clear politic maxtime is setup with ' + \
                              str(maxtime))
