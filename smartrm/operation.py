# -*- coding: utf-8 -*-

import random
import os.path
import pickle
from datetime import datetime

from smartrm.file import File

path_operations = os.path.join(os.path.dirname(__file__), '../data/operations')


def load_operations():
    operations = []

    try:
        with open(path_operations, 'r') as file:
            operations = pickle.load(file)
    except EOFError:
        operations = list()

    return operations


def save_operations(operations):
    with open(path_operations, 'w') as file:
        pickle.dump(operations, file)


class Operation(object):
    def __init__(self):
        self.id = random.randint(10 ** 20, 10 ** 21 - 1)
        self.data = datetime.now()
        self.files = list()

    def add(self, path):
        self.files.append(File().get_by_file(path))

    def add_file(self, file):
        self.files.append(file)

    def get_paths(self):
        paths = list()
        for file in self.files:
            paths.append(file.path)

        return paths

    def show(self):
        print 'ID :', self.id
        if 'trash_path' in dir(self):
            print 'Trash :', self.trash_path
            
        print 'Time :', datetime.strftime(self.data, '%Y.%m.%d %H:%M:%S')
        print 'Files :'
        for file in self.files:
            print((' '* 4) + file.path)
        print '_'*26

