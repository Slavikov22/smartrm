# -*- coding: utf-8 -*-

import os
import os.path

from smartrm.rmerror import RMError

def politic(politic):
    def interactive(func):
        def wrapper(path, trash, *args):
            if not os.path.exists(path):
                raise RMError(path + ' : Not found')

            if not os.access(path, os.R_OK) and os.path.isdir(path):
                raise RMError(path +': Permission denied')

            typefile = 'directory' if os.path.isdir(path) else 'file'

            if trash:
                print 'Do you want to move', typefile, \
                      path, 'to trash?'
            else:
                print 'Do you want to delete', typefile, \
                      path, '?'

            answer = raw_input()

            if answer == 'y':
                return func(path, trash, *args)
            else:
                return 1

        return wrapper


    def check_system(func):
        def wrapper(path, trash, *args):
            if not os.access(path, os.R_OK) and os.path.isdir(path):
                raise RMError(path +': Permission denied')

            if not os.access(path, os.R_OK):
                if trash:
                    print 'Do you want to move system file', \
                          path, 'to trash?'
                else:
                    print 'Do you want to delete system file', \
                          path, '?'

                answer = raw_input()

                if answer == 'y':
                    return func(path, trash, *args)
                else:
                    return 1
            else:
                return func(path, trash, *args)

        return wrapper


    def force(func):
        def wrapper(path, trash, *args):
            return func(path, trash, *args)

        return wrapper


    if politic == 'interactive':
        return interactive
    if politic == 'check_system':
        return check_system
    if politic == 'force':
        return force
