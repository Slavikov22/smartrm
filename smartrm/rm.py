# -*- coding: utf-8 -*-

import os
import os.path
import re
import logging
import shutil
import multiprocessing
import time
import sys

import smartrm.rm_politics as rm_politics
import smartrm.restore_politics as restore_politics
from smartrm.operation import Operation
from smartrm.file import File
from smartrm.helpers.checker import check_file, check_dir
from smartrm.rmerror import RMError


operation = Operation()


def remove(path, trash=None, dry_run=False, operation_queue=None, files_queue=None):
    if os.path.isfile(path):
        return remove_file(path, trash, dry_run, operation_queue, files_queue)
    elif os.path.isdir(path):
        return remove_tree(path, trash, dry_run, operation_queue, files_queue)


def remove_directory(path, trash=None, dry_run=False, operation_queue=None, files_queue=None):
    global operation

    logger = logging.getLogger('SmartRM')

    check_dir(path)

    if dry_run:
        operation = operation_queue.get() if operation_queue else operation
        try:
            for subpath in _get_files(path):
                if subpath not in operation.get_paths():
                    raise RMError(path + ' : Directory is not empty')
        finally:
            if operation_queue:
                operation_queue.put(operation) 
    else:
        if os.listdir(path):
            raise RMError(path + ' : Directory is not empty')

    if trash:
        if not dry_run:
            file = File().get_by_file(path)

            index = trash.add(path, files_queue)

            operation = operation_queue.get() if operation_queue else operation
            operation.add_file(file)
            operation.files[-1].index = index
            if operation_queue:
                operation_queue.put(operation)
        else:
            if path.startswith(trash.location) or \
               trash.location.startswith(path):
                raise RMError('Try to delete trash files')

            if os.path.isdir(path) and not os.access(path, os.R_OK):
                raise RMError(path + ' : Permission denied')

            logger.info(path + ' : Successfully move to trash')
    else:
        operation = operation_queue.get() if operation_queue else operation
        operation.add(path)
        if operation_queue:
            operation_queue.put(operation)

        if not dry_run:
            os.rmdir(path)

        logger.info(path + ' : Directory deleted')

    return 0


def remove_file(path, trash=None, dry_run=False, operation_queue=None, files_queue=None):
    global operation

    logger = logging.getLogger('SmartRM')

    check_file(path)

    if trash:
        if not dry_run:
            file = File().get_by_file(path)

            index = trash.add(path, files_queue)

            operation = operation_queue.get() if operation_queue else operation
            operation.add_file(file)
            operation.files[-1].index = index
            if operation_queue:
                operation_queue.put(operation)
        else:
            if path.startswith(trash.location):
                raise RMError('Try to delete trash files')

            operation = operation_queue.get() if operation_queue else operation
            operation.add(path)
            if operation_queue:
                operation_queue.put(operation)

            logger.info(path + ' : Successfully move to trash')
    else:
        operation = operation_queue.get() if operation_queue else operation
        operation.add(path)
        if operation_queue:
            operation_queue.put(operation)

        if not dry_run:
            os.remove(path)

        logger.info(path + ' : File deleted')

    return 0


def remove_tree(path, trash=None, dry_run=False, silent=False, operation_queue=None, files_queue=None):
    global operation

    logger = logging.getLogger('SmartRM')

    check_dir(path)

    for root, dirs, files in os.walk(path, topdown=False):
        for file in files:
            try:
                subpath = os.path.join(root, file)
                remove_file(subpath, trash, dry_run, operation_queue, files_queue)
            except RMError as exc:
                logger.error(exc.message)

                if not silent:
                    print exc.message

        for directory in dirs:
            subpath = os.path.join(root, directory)
            if os.path.exists(subpath) and not os.access(subpath, os.R_OK):
                msg = subpath + ' : Permission denied'
                logger.error(msg)

                if not silent:
                    print msg

        try:
            remove_directory(root, trash, dry_run, operation_queue, files_queue)
        except RMError as exc:
            logger.error(exc.message)

            if not silent:
                print exc.message

    return 0


def remove_from_regex(path, pattern, trash=None, dry_run=False, silent=False, operation_queue=None, files_queue=None):
    global operation

    is_main = False

    if not operation_queue:
        is_main = True
        operation_queue = multiprocessing.Queue()
        operation_queue.put(operation)

    if trash and not files_queue:
        files_queue = multiprocessing.Queue()
        files_queue.put(trash._files)

    logger = logging.getLogger('SmartRM')

    check_dir(path)

    threads = []

    for filename in os.listdir(path):
        subpath = os.path.join(path, filename)
        if os.path.isfile(subpath) and _is_equal(subpath, pattern):
            try:
                remove_file(subpath, trash, dry_run, operation_queue, files_queue)
            except RMError as exc:
                logger.error(exc.message)

                if not silent:
                    print exc.message

        if os.path.isdir(subpath):
            if _is_equal(subpath, pattern):
                try:
                    threads.append(multiprocessing.Process(
                        target=remove_tree,
                        name=subpath,
                        args=(subpath, trash, dry_run, silent, operation_queue, files_queue)
                    ))
                    threads[-1].start()
                except RMError as exc:
                    logger.error(exc.message)
            else:
                remove_from_regex(subpath, pattern, trash, dry_run, silent, operation_queue, files_queue)

    for thread in threads:
        thread.join()

    if is_main and not operation_queue.empty():
        operation = operation_queue.get()

    return 0


def restore(operation, trash, dry_run):
    logger = logging.getLogger('SmartRM')

    if operation.trash_path != trash.location:
        raise RMError(str(operation.id) + \
                      " : Trash can't process this operation")

    logger.info('Operation ' + str(operation.id) + ' restoring...')

    for file in operation.files:
        if os.path.exists(file.path) and not os.access(file.path, os.R_OK):
            raise RMError(file.path + ' : Permission denied')

    check_collisions = restore_politics.politic(trash.config.choise_politic)
    check_collisions(operation)

    operation.files.reverse()

    for file in operation.files:
        try:
            if not dry_run:
                trash.pop(file.name, file.index)
            else:
                logger.info(file.name + ' : Try to restore...')

                if not file.name in trash._files.keys() or \
                   len(trash._files[file.name]) <= file.index:
                    raise RMError(file.name + '[{0}]'.format(file.index) + \
                                  ' : Not found')

                logger.info(file.name + '[{0}]'.format(file.index) + \
                            ' : Restoring successful')
        except RMError as exc:
            logger.error(exc.message)

    logger.info('Operation ' + str(operation.id) + ' restored')

    return 0


def delete_operation(operation, trash, dry_run):
    logger = logging.getLogger('SmartRM')

    if operation.trash_path != trash.location:
        raise RMError(str(operation.id) + \
                      " : Trash can't process this operation") 

    for file in operation.files:
        if not os.access(trash._files_location + file.filename + \
                         '[{0}]'.format(file.index), os.R_OK):
            raise RMError(file.filename + '[{0}]'.format(file.index) + \
                          ' : Permission denied')       

    for file in operation.files:
        try:
            if not dry_run:
                trash.delete(file.name, file.index)
            else:
                if not file.name in trash._files.keys() or \
                   len(trash._files[file.name]) <= file.index:
                    raise RMError(file.name + '[{0}]'.format(file.index) + \
                          ' : Not found')

                self.logger.info(file.name + '[{0}]'.format(file.index) + \
                                 ' : Deleting successful')
        except RMError as exc:
            logger.error(exc.message)

    logger.info('Operation ' + str(operation.id) + ' deleted')

    return 0


def setup_politic(politic):
    global remove_directory
    global remove_file

    remove_directory = rm_politics.politic(politic)(remove_directory)
    remove_file = rm_politics.politic(politic)(remove_file)


def _is_equal(path, pattern):
    path = path.rstrip('/')
    name = os.path.basename(path)

    res = re.match(pattern, name)
    if res is not None:
        res = res.group(0)

    return res == name


def _get_files(path):
    result = []
    for root, dirs, files in os.walk(path):
        for file in files:
            result.append(os.path.join(root, file))

        for directory in dirs:
            result.append(os.path.join(root, directory))

    return result
