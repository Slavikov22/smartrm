# -*- coding: utf-8 -*-

import os
import os.path
import re
import time

def clear_checker(politic, value):
    def maxsize_checker(file):
        if file.size > value:
            return True
        else:
            return False


    def maxtime_checker(file):
        if time.time() - file.deletion_time > value:
            return True
        else:
            return False

    if politic == 'maxsize':
        return maxsize_checker
    if politic == 'maxtime':
        return maxtime_checker
