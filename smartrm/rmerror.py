# -*- coding: utf-8 -*-

class RMError(Exception):
    def __init__(self, message):
        self.message = message