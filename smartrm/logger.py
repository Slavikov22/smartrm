# -*- coding: utf-8 -*-

import logging
import sys
import os.path
import os

from smartrm.rmerror import RMError


def init(level=logging.CRITICAL, filename=None):
    logger = logging.getLogger('SmartRM')
    logger.setLevel(level)

    if filename:
        if os.path.exists(filename):
            if not os.path.isfile(filename):
                raise RMError(filename + ' : Invalid log file')

            if not os.access(filename, os.W_OK):
                raise RMError(filename + ' : Denied access to log file')


        handler = logging.FileHandler(filename, mode='w')
    else:
        handler = logging.StreamHandler()

    formatter = logging.Formatter(
        '%(asctime)s - %(levelname)s - %(message)s')

    handler.setFormatter(formatter)

    logger.addHandler(handler)
