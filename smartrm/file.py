# -*- coding: utf-8 -*-

import os
import os.path
import time
import ConfigParser

from smartrm.rmerror import RMError


class File(object):
    def get_by_file(self, path):
        path = path.rstrip('/')

        if not os.path.exists(path):
            raise RMError(path + ' : File not found')

        statinfo = os.stat(path)

        self.path = os.path.abspath(path)
        self.name = os.path.basename(path)
        self.size = os.path.getsize(path)
        self.time_change = int(os.path.getmtime(path))
        self.deletion_time = int(time.time())

        return self


    def get_by_info(self, path):
        path = path.rstrip('/')

        if not os.path.exists(path):
            raise RMError(path + ' : File not found')

        if not os.path.isfile(path):
            raise RMError(path + ' : Is not file')

        if not os.access(path, os.R_OK):
            raise RMError(path + ' : Permission denied')

        parser = ConfigParser.RawConfigParser()
        parser.read(path)

        self.path = parser.get('Trash Info', 'path')
        self.name = parser.get('Trash Info', 'name')
        self.size = int(parser.get('Trash Info', 'size'))
        self.time_change = int(parser.get('Trash Info', 'time_change'))
        self.deletion_time = int(parser.get('Trash Info', 'deletion_time'))

        return self


    def get_info_parser(self):
        parser = ConfigParser.RawConfigParser()

        parser.add_section('Trash Info')
        parser.set('Trash Info', 'path', self.path)
        parser.set('Trash Info', 'name', self.name)
        parser.set('Trash Info', 'size', self.size)
        parser.set('Trash Info', 'time_change', str(self.time_change))
        parser.set('Trash Info', 'deletion_time', str(self.deletion_time))

        return parser
