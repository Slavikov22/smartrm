from setuptools import setup, find_packages

setup(
	name='SmartRM',
	version='1.1',
	author='Slava Yakovlev',
	author_email='Slavikov22@mail.ru',
	packages=find_packages(),
	data_files=[('data', ['data/operations'])],
	entry_points={
		'console_scripts': ['SmartRM = smartrm.parser:run']
	}
)